# SherpaInstaller

SherpaInstaller is a bash script to download and compile Sherpa 2.2.15 with Rivet 3.1.8, HepMC 3-3.2.6 and LHAPDF 6.5.4 inside the same location. It also set the configuration of Sherpa to work with those other programs. 
## Requirements

- texlive-full
- C++ compiler with c++11 standard support
- gfortran ≥ 4.6
- CMake version 3.X.

## Installation

To download SherpaInstaller, untar it and enter to the created folder, run
```
wget https://gitlab.com/ed_in/sherpainstaller/-/archive/main/sherpainstaller-main.tar.gz
tar -xzf sherpainstaller-main.tar.gz 
cd sherpainstaller-main/
```


Make the script executable by
```bash
chmod +x SherpaInstaller.sh
```
And run it by
```bash
./SherpaInstaller.sh $LOCATION
```

Where $LOCATION is the path where you want to make the installation, if the directory does not exists, the script will make it.

## Usage

Before compiling Sherpa, it requests to press enter in order to check if everything went well with the installation and configuration from the other software.

## Software Sources

[Sherpa](https://sherpa-team.gitlab.io/)  
[Rivet](https://rivet.hepforge.org/)  
[HepMC](https://gitlab.cern.ch/hepmc/HepMC3)  
[LHAPDF](https://lhapdf.hepforge.org/)  
[Yoda](https://yoda.hepforge.org/)  
[OpenLoops](https://openloops.hepforge.org/) 





