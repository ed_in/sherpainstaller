#!/usr/bin/env bash

##!/bin/bash

location="$1"

# Check if the user added a location
if [ -z "$location" ]; then
  #To take location where the script is
  location=`pwd`
  echo "Location not specified, Sherpa will be installed in $location."
fi

# Check if the location already exists
if [ ! -d "$location" ]; then
  mkdir -p "$location"
fi

cd $location

#Installing Rivet

wget https://rivet.hepforge.org/downloads/?f=Rivet-3.1.8.tar.gz
tar -xzf index.html?f=Rivet-3.1.8.tar.gz
cd Rivet-3.1.8

wget https://gitlab.com/hepcedar/rivetbootstrap/raw/3.1.8/rivet-bootstrap

chmod +x rivet-bootstrap

./rivet-bootstrap
rm rivet-bootstrap
cd ..

read -p "Press enter to continue"

#Installing HepMC3

wget http://hepmc.web.cern.ch/hepmc/releases/HepMC3-3.2.6.tar.gz

tar -xzf HepMC3-3.2.6.tar.gz

mkdir hepmc3-build

cd hepmc3-build/

cmake -DCMAKE_INSTALL_PREFIX=../hepmc3-install           -DHEPMC3_ENABLE_ROOTIO:BOOL=OFF                    -DHEPMC3_ENABLE_PROTOBUFIO:BOOL=OFF                -DHEPMC3_ENABLE_TEST:BOOL=OFF                      -DHEPMC3_INSTALL_INTERFACES:BOOL=ON                -DHEPMC3_BUILD_STATIC_LIBS:BOOL=OFF                -DHEPMC3_BUILD_DOCS:BOOL=OFF             -DHEPMC3_ENABLE_PYTHON:BOOL=ON           -DHEPMC3_PYTHON_VERSIONS=2.7             -DHEPMC3_Python_SITEARCH27=../hepmc3-install/lib/python2.7/site-packages         ../HepMC3-3.2.6

make -j8 && make install

cd ..


#Installing OpenLoops

git clone https://gitlab.com/openloops/OpenLoops.git
cd OpenLoops

./scons

cd ..

#Installing LHAPDF
#LHAPDF, the version LHAPDF-6.X.Y.tar.gz can be changed
wget https://lhapdf.hepforge.org/downloads/?f=LHAPDF-6.5.4.tar.gz -O LHAPDF-6.5.4.tar.gz
# ^ or use a web browser to download, which will get the filename correct
tar xf LHAPDF-6.5.4.tar.gz
cd LHAPDF-6.5.4
./configure --prefix=$location/LHAPDF/local
make -j8
make install 
cd ..


#Installing Sherpa
wget https://sherpa.hepforge.org/downloads/SHERPA-MC-2.2.15.tar.gz
tar -zxf SHERPA-MC-2.2.15.tar.gz


cd SHERPA-MC-2.2.15

./configure  --enable-rivet=$location/Rivet-3.1.8/local --enable-hepmc3=$location/hepmc3-install  --enable-openloops=$location/OpenLoops --enable-lhapdf=$location/LHAPDF/local --with-sqlite3=install  --enable-pythia  --enable-gzip --no-recursion 

read -p "Press enter to continue"

make install -j8

cd ..

rm -r -f index.html?f=Rivet-3.1.8.tar.gz  SHERPA-MC-2.2.15.tar.gz LHAPDF-6.5.4.tar.gz LHAPDF-6.5.4 HepMC3-3.2.6.tar.gz HepMC3-3.2.6 hepmc3-build
